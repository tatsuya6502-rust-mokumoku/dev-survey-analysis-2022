# Dev Survey Analysis 2022

このリポジトリーには、以下の2つの開発者アンケートの集計結果を比較した際に使用したJupyterノートブックが保存されています。

- [Stack Overflow Developer Survey 2022](https://survey.stackoverflow.co/2022/)
- [Qiita エンジニア白書2022](https://corp.qiita.com/releases/2022/12/qiita-white-papers-2022/)

## ライセンス / License

MITライセンス
